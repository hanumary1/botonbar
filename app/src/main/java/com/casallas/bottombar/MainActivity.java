package com.casallas.bottombar;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Switch;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    FrameLayout miContenedor;
    BottomNavigationView menuInferior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuInferior=findViewById(R.id.menuInferior);

        //listener para eventos de los iconos del menu
        //requiere argumento de la clase navigation bar view

        menuInferior.setOnItemSelectedListener(
                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        //aqui se programa lo que hace clic en algun item
                        //Obtener el ID del item seleccionado
                        int id=item.getItemId();
                        Log.d(TAG,String.valueOf(id));
                        switch (id) {

                            case R.id.iCalc:
                                //para cargar el fragmento de calculadora
                                //mannager y transaction
                                FragmentManager miAdmin;
                                FragmentTransaction miTrans;
                                miAdmin = getSupportFragmentManager();
                                miTrans = miAdmin.beginTransaction();
                                miTrans.replace(R.id.contenedorFragmentos, new FragmentoCalculadora());
                                miTrans.commit();

                                break;

                            case R.id.iTeams:
                                //para cargar el fragmento de equipos
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragmentos, new FragmentoEquipos()).commit();
                                break;

                            case R.id.iMap:
                                //para cargar el fragmento de mapa
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragmentos, new FragmentoMapa()).commit();
                                break;
                        }
                        return false;
                    }

                }
        );
    }
}
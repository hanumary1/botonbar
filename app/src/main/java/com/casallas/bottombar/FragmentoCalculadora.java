package com.casallas.bottombar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.zip.Inflater;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentoCalculadora#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoCalculadora extends Fragment implements View.OnClickListener{

    MaterialButton button1, button2, button3,button4, button5,button6,button7,button8,button9;
    MaterialButton buttonAC, buttonC, button_igual,button_parent_open, button_punt,button_parent_close;
    MaterialButton button_div, button_mult, button_men,button_plus, button0;
    TextView expresion,resultado;


    public FragmentoCalculadora() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentoCalculadora newInstance(String param1, String param2) {
        FragmentoCalculadora fragment = new FragmentoCalculadora();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragmento_calculadora,container,false);

        expresion=view.findViewById(R.id.expresion);
        resultado=view.findViewById(R.id.resultado);
        button1=view.findViewById(R.id.button1);
        button2=view.findViewById(R.id.button2);
        button3=view.findViewById(R.id.button3);
        button4=view.findViewById(R.id.button4);
        button5=view.findViewById(R.id.button5);
        button6=view.findViewById(R.id.button6);
        button7=view.findViewById(R.id.button7);
        button8=view.findViewById(R.id.button8);
        button9=view.findViewById(R.id.button9);
        button0=view.findViewById(R.id.button0);
        buttonC=view.findViewById(R.id.buttonC);
        buttonAC=view.findViewById(R.id.buttonAC);
        button_igual=view.findViewById(R.id.button_igual);
        button_parent_open=view.findViewById(R.id.button_parent_open);
        button_parent_close=view.findViewById(R.id.button_parent_close);
        button_punt=view.findViewById(R.id.button_punt);
        button_plus=view.findViewById(R.id.button_plus);
        button_men=view.findViewById(R.id.button_men);
        button_mult=view.findViewById(R.id.button_mult);
        button_div=view.findViewById(R.id.button_div);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button0.setOnClickListener(this);
        buttonAC.setOnClickListener(this);
        button_div.setOnClickListener(this);
        button_punt.setOnClickListener(this);
        button_mult.setOnClickListener(this);
        button_men.setOnClickListener(this);
        button_plus.setOnClickListener(this);
        button_parent_close.setOnClickListener(this);
        button_parent_open.setOnClickListener(this);
        button_igual.setOnClickListener(this);
        buttonC.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View view) {
        MaterialButton boton= (MaterialButton) view;
        String texto;
        texto=boton.getText().toString();
        //Cadena para concatenar texto
        String cadena=expresion.getText().toString();

        if (texto.equals("C")){
            expresion.setText("0");
            return;
        }
        if (texto.equals("AC")){
            expresion.setText("0");
            resultado.setText("0");
            return;
        }


        if (texto.equals(".")){
        }else if (cadena.equals("0")){
            cadena="";
        }
        //Ciclo para obtener el resultado de las operaciones
        if (texto.equals("=")){
            Expression analizador= new Expression(cadena);
            Double operacion= analizador.calculate();
            if (Double.isNaN(operacion)){
                resultado.setText("Operación incorrecta!");
            }else{
                resultado.setText(Double.toString(operacion));
            }
        }else {
            //concatenar lo digitado
            cadena = cadena + texto;
            expresion.setText(cadena);
        }

    }
}